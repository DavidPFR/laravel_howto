#!/bin/bash
# A menu driven shell script sample template 
## ----------------------------------
# Step #1: Define variables
# ----------------------------------
EDITOR=vim
PASSWD=/etc/passwd
RED='\033[0;41;30m'
STD='\033[0;0;39m'
 
# ----------------------------------
# Step #2: User defined function
# ----------------------------------
pause(){
  read -p "Press [Enter] key to continue...🐱‍🏍" fackEnterKey
}

createBackEndLaravelProject(){
    
    DIR="$PWD/app/back"
    if [ -d "$DIR" ]; then
    
        if [ -e "$DIR" ]; then

          echo "Project directory "$DIR" is not empty."
          sleep 2
          show_menus
        fi
        mkdir -p $DIR
        return true
    fi
	docker run --rm --interactive --tty --volume $PWD:/app --user $(id -u):$(id -g)  composer create-project laravel/laravel app/back
    show_menus
} 
 
# do something in two()
two(){
	echo "two() called"
        pause2
}

# do something in two()
buildDockerImage(){
	echo "Compilation en cours..." & \
	USER=$USER USER_ID=$(id -u) GROUP_ID=$(id -g) docker-compose build  --parallel &>/dev/null &&\
	docker-compose create web phpmyadmin mysql
    sleep 3
    show_menus
}
 
# do something in two()
deleteDockerImage(){
    docker-compose stop 
    docker system prune -a
    sleep 3
    show_menus
}

# do something in two()
composerInstall(){
	if [ $( docker ps -a | grep pepite_php | wc -l ) -gt 0 ]; then
		 docker run --rm --interactive --tty --volume $PWD/app/back:/app --user $(id -u):$(id -g)  composer install
		docker run --user $(id -u):$(id -g)  -ti  -v $PWD/app/back:/app pepite_php php artisan key:generate
	else
	  echo "😒pepite_php does not exist"
	fi
    sleep 3
    show_menus
}

# do something in two()
startProject(){
    USER=$USER USER_ID=$(id -u) GROUP_ID=$(id -g) docker-compose up -d
    sleep 3
    show_menus
}
 
# function to display menus
show_menus() {
	clear
	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " M A I N - M E N U"
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo "1. Create a Laravel BackEnd Project🐱‍🏍"
	echo "2. Build docker's images ( docker-build )"
	echo "3. Start project"
	echo "4. Delete docker's images 🤧"
	echo "5. Install backend's dependencies"
	echo "6. Kamolux😍"
	echo "0. Exit"
}
# read input from the keyboard and take a action
# invoke the one() when the user select 1 from the menu option.
# invoke the two() when the user select 2 from the menu option.
# Exit when user the user select 3 form the menu option.
read_options(){
	local choice
	read -p "Enter choice [ 1 - 5] " choice
	case $choice in
		0) exit 0;;
		1) createBackEndLaravelProject ;;
		2) buildDockerImage;;
		3) startProject;;
		4) deleteDockerImage;;
		5) composerInstall;;
		6) telnet towel.blinkenlights.nl;;
		*) echo -e "${RED}Error...${STD}" && sleep 2
	esac
}
 
# ----------------------------------------------
# Step #3: Trap CTRL+C, CTRL+Z and quit singles
# ----------------------------------------------
trap '' SIGINT SIGQUIT SIGTSTP
 
# -----------------------------------
# Step #4: Main logic - infinite loop
# ------------------------------------
while true
do
 
	show_menus
	read_options
done
